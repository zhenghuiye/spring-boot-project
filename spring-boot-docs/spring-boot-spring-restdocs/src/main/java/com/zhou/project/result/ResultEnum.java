package com.zhou.project.result;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-04 16:33
 * @description: TODO
 */
@Getter
public enum ResultEnum {
    INTERFACE_CALL_SUCCESS(2000, "接口调用成功"),
    INTERFACE_CALL_ERROR(2001, "接口调用异常");
    @Setter
    private Integer code;
    @Setter
    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
