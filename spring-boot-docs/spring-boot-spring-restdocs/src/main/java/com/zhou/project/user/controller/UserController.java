package com.zhou.project.user.controller;

import com.zhou.project.user.entity.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-03 16:32
 * @description: TODO
 */
@RestController
@RequestMapping("user")
public class UserController {

    @PostMapping("save")
    public User save(@RequestBody User entity)
    {
        return entity;
    }

    @PostMapping("update")
    public User update(@RequestBody User entity)
    {
        return entity;
    }

}
