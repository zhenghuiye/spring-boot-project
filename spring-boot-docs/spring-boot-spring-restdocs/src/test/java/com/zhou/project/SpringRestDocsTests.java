package com.zhou.project;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhou.project.user.entity.User;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;
import java.util.Date;

import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-04 15:59
 * @description: TODO
 */
@ExtendWith({
    SpringExtension.class,
    RestDocumentationExtension.class
})
@SpringBootTest
public class SpringRestDocsTests {

    private MockMvc mockMvc;

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private WebApplicationContext context;

    //模拟登陆token
    private static final String AUTHORIZATION = "Bearer fa59a03e-aa3e-4d2e-bf43-2f0b739ec4bf";

    @BeforeEach
    public void setUp(RestDocumentationContextProvider restDocumentationContextProvider) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(restDocumentationContextProvider)).build();
    }

    @SneakyThrows
    @Test
    public void generate()
    {
        //模拟客户端数据
        User entity = new User();
        entity.setName("小薇");
        entity.setGender(2);
        entity.setBirthday(new Date());
        entity.setAddress("北京市海淀区");

        this.mockMvc.perform(
            post("/user/save")
                .header("Authorization", AUTHORIZATION)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(entity))
            )
            .andExpect(status().isOk())
            .andDo(
                document(
                "user/save",
                    requestHeaders(
                        headerWithName("Authorization").description("用户鉴权token")
                    ),
                    requestFields(
                        fieldWithPath("id").ignored(),
                        fieldWithPath("name").description("用户名称").type(JsonFieldType.STRING),
                        fieldWithPath("gender").description("用户性别").type(JsonFieldType.NUMBER),
                        fieldWithPath("birthday").description("出生日期").type(JsonFieldType.STRING),
                        fieldWithPath("address").description("手机号码").type(JsonFieldType.STRING)
                    )
                )
            );
    }

}
