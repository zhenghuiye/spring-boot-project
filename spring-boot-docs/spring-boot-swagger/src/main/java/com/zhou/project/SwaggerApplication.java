package com.zhou.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-03 16:33
 * @description: TODO
 */
@SpringBootApplication
public class SwaggerApplication {
    public static void main(String[] args) {
        //swagger3 暂时未对 springboot3 支持
        SpringApplication.run(SwaggerApplication.class, args);
    }
}
