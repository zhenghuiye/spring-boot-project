package com.zhou.project.user.controller;

import com.zhou.project.user.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Date;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-03 16:32
 * @description: TODO
 */
@RestController
@Api(tags = "用户接口")
@RequestMapping("user")
public class UserController {

    @ApiOperation(value = "注册用户")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "name", value = "姓名", required = true, dataTypeClass = String.class),
        @ApiImplicitParam(name = "gender", value = "性别", required = true, dataTypeClass = Integer.class),
        @ApiImplicitParam(name = "birthday", value = "生日", required = true, dataTypeClass = Date.class),
        @ApiImplicitParam(name = "address", value = "住址", dataTypeClass = String.class)
    })
    @PostMapping("save")
    public void save(@RequestBody @ApiIgnore User entity)
    {
    }

    @ApiOperation(value = "编辑用户")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "主键", required = true, dataTypeClass = Integer.class),
        @ApiImplicitParam(name = "name", value = "姓名", required = true, dataTypeClass = String.class),
        @ApiImplicitParam(name = "gender", value = "性别", required = true, dataTypeClass = Integer.class),
        @ApiImplicitParam(name = "birthday", value = "生日", required = true, dataTypeClass = Date.class),
        @ApiImplicitParam(name = "address", value = "住址", dataTypeClass = String.class)
    })
    @PutMapping("update")
    public void update(@RequestBody User entity)
    {
    }

}
