package com.zhou.project.moduels.user.service;

import com.zhou.project.moduels.user.entity.User;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-08 15:45
 * @description: TODO
 */
@Service
public class UserService {

    /**
     * 模拟查询返回用户信息
     * @param name 登录账号
     * @return User
     */
    public User findUserByName(String name){
        User user = new User();
        user.setName(name);
        user.setNick("NICK");
        //密码明文是123456
        user.setPass("J/ms7qTJtqmysekuY8/v1TAS+VKqXdH5sB7ulXZOWho=");
        //加密密码的盐值
        user.setSalt("wxKYXuTPST5SG0jMQzVPsg==");
        return user;
    }

    /**
     * 模拟根据账号查询返回用户的所有角色
     * @param username 登录账号
     * @return Set<String> 角色集合
     */
    public Set<String> getRolesByUser(String username){
        Set<String> roles = new HashSet<>();
        if("admin".equals(username))
            roles.add("ADMIN");
        else
            roles.add("USERS");
        return roles;
    }

    /**
     * 模拟根据账号查询返回用户的所有权限
     * @param username 登录账号
     * @return Set<String> 权限集合
     */
    public Set<String> getPermsByUser(String username) {
        Set<String> perms = new HashSet<>();
        perms.add("user:select");
        if ("admin".equals(username)) {
            perms.add("user:insert");
            perms.add("user:update");
            perms.add("user:delete");
        }
        return perms;
    }

}
