package com.zhou.project.relation.service;

import com.zhou.project.relation.dao.EmployeesDao;
import com.zhou.project.relation.entity.Department;
import com.zhou.project.relation.entity.Employees;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 12:19
 * @description: TODO
 */
@Service
@Transactional(readOnly = true)
public class EmployeesService {

    @Autowired
    private EmployeesDao employeesDao;

    /**
     * 保证逻辑层同一事务单元 ACID 原子性
     * @param entity 员工对象(包含部门)
     */
    @Transactional
    public void save(Employees entity)
    {
        //首先保存部门  @ManyToOne(cascade = CascadeType.PERSIST)
        //获取部门主键插入员工表 部门外键  @JoinColumn(name = "department_id")
        employeesDao.save(entity);
    }

    /**
     * 主键查询员工对象
     * @param id 员工主键
     * @return 员工对象
     */
    public Employees load(int id)
    {
        return employeesDao.findById(id).get();
    }

}
