package com.zhou.project.simple.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-26 16:38
 * @description: [用户实体对象]
 */
@Getter
@Setter
@ToString
@TableName(value = "users")
public class Users {
    /**
     * 用户主键
     */
    @TableId(type = IdType.AUTO)
    private int id;
    /**
     * 用户姓名
     */
    @TableField
    private String name;
    /**
     * 用户性别
     * 1:男 2:女
     */
    @TableField
    private int gender;
    /**
     * 出生日期
     */
    @TableField
    private Date birthday;
    /**
     * 家庭住址
     */
    @TableField
    private String address;
}
