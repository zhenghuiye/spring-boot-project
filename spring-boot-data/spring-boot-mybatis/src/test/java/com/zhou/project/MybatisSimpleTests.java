package com.zhou.project;

import com.zhou.project.simple.entity.Users;
import com.zhou.project.simple.service.UsersService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.util.AssertionErrors.*;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 10:59
 * @description: Mybatis简单CRUD单元测试
 */
@SpringBootTest
public class MybatisSimpleTests {

    @Autowired
    private UsersService usersService;

    /**
     * 测试 添加 人员
     */
    @Test
    public void insert()
    {
        Users entity = new Users();
        entity.setName("小李子");
        entity.setGender(1);
        entity.setBirthday(new Date());
        entity.setAddress("北京市海淀区");

        int result = usersService.insert(entity);
        assertEquals("添加人员测试:", 1, result);
    }

    /**
     * 测试 修改 人员
     */
    @Test
    public void update()
    {
        Users entity = new Users();
        entity.setId(1);
        entity.setName("小李子1");
        entity.setGender(2);
        entity.setBirthday(new Date());
        entity.setAddress("北京市海淀区1");

        int result = usersService.update(entity);
        assertEquals("修改人员测试:", 1, result);
    }

    /**
     * 测试 删除 人员
     */
    @Test
    public void delete()
    {
        int id = 1;
        int result = usersService.delete(id);
        assertEquals("删除人员测试:", 1, result);
    }

    /**
     * 测试 批量删除 人员
     */
    @Test
    public void deletes()
    {
        int[] ids = {2, 3, 4};
        int result = usersService.deletes(ids);
        assertEquals("批量删除人员测试:", 3, result);
    }

    /**
     * 测试 主键 查询 人员
     */
    @Test
    public void load()
    {
        Users entity = usersService.queryId(5);
        assertNotNull("主键查询人员测试:", entity);
    }

    /**
     * 测试 总数 查询 人员
     */
    @Test
    public void count()
    {
        //封装查询参数
        Map<String, Object> params = new HashMap<>();
        //调用查询方法
        long count = usersService.queryCount(params);
        //测试执行结果
        assertEquals("总数查询测试:", 3L, count);
    }

    /**
     * 测试 分页 查询 人员
     */
    @Test
    public void page()
    {
        //封装查询参数
        Map<String, Object> params = new HashMap<>();
        params.put("start", 0);
        params.put("sizes", 10);
        //调用查询方法
        List<Users> list = usersService.queryPage(params);
        //测试执行结果
        list.forEach(System.out::println);
    }

}
