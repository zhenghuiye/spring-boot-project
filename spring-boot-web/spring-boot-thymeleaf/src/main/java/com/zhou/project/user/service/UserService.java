package com.zhou.project.user.service;
import java.util.Date;

import com.zhou.project.user.entity.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-02 16:56
 * @description: TODO
 */
@Service
public class UserService {

    /**
     * 模拟分页查询
     */
    public List<User> page(int current, int sizes)
    {
        List<User> list = new ArrayList<>();
        for (int i = (current-1)*sizes+1; i <= current*sizes; i++) {
            User entity = new User();
            entity.setId(i);
            entity.setName("巫师"+i);
            entity.setGender(i%2+1);
            entity.setBirthday(new Date());
            entity.setAddress("北京海淀"+i);

            list.add(entity);
        }
        return list;
    }

}
